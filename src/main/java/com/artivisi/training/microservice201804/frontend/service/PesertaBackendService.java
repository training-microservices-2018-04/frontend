package com.artivisi.training.microservice201804.frontend.service;

import com.artivisi.training.microservice201804.frontend.dto.Peserta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@FeignClient(name = "peserta", fallback = PesertaBackendFallback.class)
public interface PesertaBackendService {
    @GetMapping("/api/peserta/")
    public Iterable<Peserta> dataPeserta();

    @GetMapping("/api/host")
    Map<String, String> host();
}
