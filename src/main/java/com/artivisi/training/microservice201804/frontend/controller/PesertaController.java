package com.artivisi.training.microservice201804.frontend.controller;

import com.artivisi.training.microservice201804.frontend.dto.Peserta;
import com.artivisi.training.microservice201804.frontend.service.PesertaBackendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;

@Controller
public class PesertaController {

    @Autowired private PesertaBackendService pesertaBackendService;

    @GetMapping("/peserta/list")
    public ModelMap dataPeserta() {

        return new ModelMap()
                .addAttribute("dataPeserta",
                        pesertaBackendService.dataPeserta());
    }

    @GetMapping("/peserta/backend")
    public ModelMap informasiBackend() {
        return new ModelMap()
                .addAttribute("backendInfo",
                        pesertaBackendService.host());
    }
}
