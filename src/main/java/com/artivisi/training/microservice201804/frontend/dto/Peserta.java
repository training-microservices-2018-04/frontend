package com.artivisi.training.microservice201804.frontend.dto;

import lombok.Data;

@Data
public class Peserta {
    private String id;
    private String nomor;
    private String namaDepan;
    private String namaBelakang;
    private String email;
    private String noHp;
    private String foto;

    public String getNamaLengkap() {
        return namaDepan + " " + namaBelakang;
    }
}
